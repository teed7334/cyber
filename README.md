# cyber
思雲達面試用考題

## 考題
請寫出一個API server.
此server能存取以下data:

```
type Data struct {
    ID string
    Location struct{
        Lat float32
        Long float32
    }
    DateAdded time.Time
}
```

以下條件
1. 使用Go開發，不使用Framework (但可使用library, 像是database lib: sqlx)
2. 使用MySQL Database
3. 有二個endpoint
- GET /data → 回傳所有database裡面的data
- POST /data → 把上傳的data存入database

以下為加分:
1. 使用 Docker and/or Docker Compose
2. Unit testing
3. 良好的檔案和程式架構

## 資料夾結構
./controller 控制器

./dev_env Docker Compose相關設定檔

./dto 資料傳輸元件

./helper 開發用小幫手

./service 服務器

./test 程式測試碼

./.env 設定檔

./.go.mod Golang Module 設定檔

./main.go 主程式

## 資料流
User -> Borwser -> HTTP Request -> main.go -> 分配到相應之控制器 -> 初步檢查 HTTP Request 參數都有帶齊 -> 傳給服務器 -> 執行商業邏輯 -> 調用運行商業邏輯所需之函式庫 -> 回傳結果 -> 控制器 -> 輸出 HTTP Response -> Borwser -> User

## 使用說明
1. 先進入dev_env啟用Docker Compose
2. 執行本程式
```
go run main.go
```
3. 運行migration，打開瀏覽器，進入以下網址
[http://localhost:20080/init/setup](http://localhost:20080/init/setup)

## Todo
無

## API

新增定位
# HTTP POST
url: http://[Path To]/location/add

# 參數 
```
{
    "id":"Peter Cheng",
    "location": {
        "lat": 23.546162,
        "long": 120.6402133
    }
}
```

# 回傳 
```
ok
```

取得全部定位資料
# HTTP GET
url: http://[Path To]/location/get

# 參數
N/A

# 回傳
```
[
    {
        "id":"Peter Cheng",
        "Location":{"lat":23.546162,"long":120.64021},
        "DateAdded":"2020-08-25T20:05:28+08:00"
    },
    ...
]
```
