package controller

import (
	"cyber/dto"
	"cyber/service"
	"encoding/json"
	"fmt"
	"net/http"
)

//Location 坐標定位物件
type Location struct{}

//New 建構式
func (l Location) New() *Location {
	return &l
}

//Add 新增資料
func (l *Location) Add(w http.ResponseWriter, r *http.Request) {
	dto := &dto.LocationDto{}
	err := json.NewDecoder(r.Body).Decode(dto)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	err = service.Location{}.New().Add(dto)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	fmt.Fprintf(w, "ok")
}

//Get 取得所有資料
func (l *Location) Get(w http.ResponseWriter, r *http.Request) {
	dto := &dto.LocationDto{}
	_, err := service.Location{}.New().Get(dto)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	results, err := service.Location{}.New().Get(dto)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	jsonByte, err := json.Marshal(results)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	fmt.Fprintf(w, string(jsonByte))
}
