package controller

import (
	"cyber/service"
	"fmt"
	"net/http"
)

//Migrate 新增所需之資料表
type Migrate struct{}

//New 建構式
func (m Migrate) New() *Migrate {
	return &m
}

//Setup 初始化資料表
func (m *Migrate) Setup(w http.ResponseWriter, r *http.Request) {
	err := service.Migrate{}.New().Setup()
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	fmt.Fprintf(w, "ok")
}
