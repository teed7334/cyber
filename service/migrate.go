package service

import (
	"cyber/helper"
)

//Migrate 新增所需之資料表
type Migrate struct{}

//New 建構式
func (m Migrate) New() *Migrate {
	return &m
}

//Setup 初始化資料表
func (m *Migrate) Setup() error {
	sql := `
		CREATE TABLE cyber.` + "`data`" + ` (
			id varchar(100) NULL COMMENT '主鍵',
			location point NULL COMMENT '經瑋度',
			date_added TIMESTAMP NULL COMMENT '新增時間'
		)
		ENGINE=InnoDB
		DEFAULT CHARSET=utf8mb4
		COLLATE=utf8mb4_0900_ai_ci; 
	`
	db := helper.MySQL{}.New().GetAdapter()
	_, err := db.Exec(sql)
	defer db.Close()
	return err
}
