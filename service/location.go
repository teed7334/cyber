package service

import (
	"cyber/dto"
	"cyber/helper"
	"fmt"
	"os"
	"time"
)

//Location 坐標定位物件
type Location struct{}

//New 建構式
func (l Location) New() *Location {
	return &l
}

//Add 新增資料
func (l Location) Add(dto *dto.LocationDto) error {
	timeFormat := os.Getenv("timeFormat")
	now := time.Now().Format(timeFormat)
	sql := "INSERT INTO data values(?, ST_GeomFromText(?), ?)"
	db := helper.MySQL{}.New().GetAdapter()
	point := fmt.Sprintf("POINT(%g %g)", dto.Location.Lat, dto.Location.Long)
	_, err := db.Exec(sql, dto.ID, point, now)
	defer db.Close()
	return err
}

//Get 取得所有資料
func (l *Location) Get(ld *dto.LocationDto) ([]*dto.LocationDto, error) {
	sql := "SELECT id, ST_X(location) AS lat, ST_Y(location) AS 'long', date_added FROM data"
	db := helper.MySQL{}.New().GetAdapter()
	rows, err := db.Query(sql)
	if err != nil {
		return nil, err
	}
	var dateAdded string
	var results []*dto.LocationDto
	timeFormat := os.Getenv("timeFormat")
	for rows.Next() {
		rows.Scan(&ld.ID, &ld.Location.Lat, &ld.Location.Long, &dateAdded)
		ld.DateAdded, _ = time.ParseInLocation(timeFormat, dateAdded, time.Local)
		results = append(results, ld)
	}
	return results, nil
}
