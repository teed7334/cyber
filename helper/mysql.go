package helper

import (
	"database/sql"
	"fmt"
	"log"
	"os"

	_ "github.com/go-sql-driver/mysql"
)

//MySQL 物件參數
type MySQL struct {
	user     string
	passwd   string
	host     string
	port     string
	database string
	adapter  *sql.DB
}

//New 建構式
func (m MySQL) New() *MySQL {
	m.user = os.Getenv("mysql_user")
	m.passwd = os.Getenv("mysql_password")
	m.host = os.Getenv("mysql_host")
	m.port = os.Getenv("mysql_port")
	m.database = os.Getenv("mysql_database")
	dsn := fmt.Sprintf("%s:%s@tcp(%s:%s)/%s", m.user, m.passwd, m.host, m.port, m.database)
	adapter, err := sql.Open("mysql", dsn)
	if err != nil {
		log.Fatal(err)
	}
	m.adapter = adapter
	return &m
}

//GetAdapter 取得資料庫元件
func (m MySQL) GetAdapter() *sql.DB {
	return m.adapter
}
