package dto

import "time"

//LocationDto 坐標定位參數
type LocationDto struct {
	ID       string `json:"id"`
	Location struct {
		Lat  float32 `json:"lat,float64"`
		Long float32 `json:"long,float64"`
	}
	DateAdded time.Time
}
