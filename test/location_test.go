package test

import (
	"cyber/dto"
	"cyber/service"
	"testing"
	"time"
)

func TestAdd(t *testing.T) {
	dto := &dto.LocationDto{}
	dto.ID = "Peter Cheng"
	dto.Location.Lat = 23.546162
	dto.Location.Long = 120.64021
	dto.DateAdded = time.Now()
	service.Location{}.New().Add(dto)
}

func TestGet(t *testing.T) {
	dto := &dto.LocationDto{}
	service.Location{}.New().Get(dto)
}
