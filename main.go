package main

import (
	"cyber/controller"
	"fmt"
	"net/http"
	"os"

	_ "github.com/joho/godotenv/autoload"
)

var location = controller.Location{}.New()
var migrate = controller.Migrate{}.New()

//middleware 中間件
func middleware(next http.Handler, method []string) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w = setCORS(w)
		status := checkHTTPMethod(r, method)
		if status != 1 {
			fmt.Fprintf(w, "不被允許的呼叫方式")
			return
		}
		next.ServeHTTP(w, r)
	})
}

//checkHTTPMethod 檢查HTTP Method
func checkHTTPMethod(r *http.Request, method []string) int {
	accept := inArray(method, r.Method)
	return accept
}

//setCORS 設定CORS
func setCORS(w http.ResponseWriter) http.ResponseWriter {
	w.Header().Set("Access-Control-Allow-Origin", "*")
	return w
}

//InArray 於陣列中有對應之值
func inArray(arr []string, str string) int {
	for _, v := range arr {
		if v == str {
			return 1
		}
	}
	return 0
}

func main() {
	port := fmt.Sprintf(":%s", os.Getenv("port"))
	handler := http.HandlerFunc(migrate.Setup)
	http.Handle("/init/setup", middleware(handler, []string{"GET"}))
	handler = http.HandlerFunc(location.Add)
	http.Handle("/location/add", middleware(handler, []string{"POST"}))
	handler = http.HandlerFunc(location.Get)
	http.Handle("/location/get", middleware(handler, []string{"GET"}))
	http.ListenAndServe(port, nil)
}
